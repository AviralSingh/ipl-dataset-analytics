import csv
import matplotlib.pyplot as plt

# Function to process and collect umpire nationalities
def process_and_plot_umpire_nationalities(matches_csv_path, umpires_csv_path):
    umpire_names = set()
    umpire_nation = {}
    
    with open(matches_csv_path, newline='') as matches_file:
        csv_reader = csv.DictReader(matches_file)
        for row in csv_reader:
            umpire_names.add(row['umpire1'])
            umpire_names.add(row['umpire2'])

    with open(umpires_csv_path, newline='') as umpires_file:
        csv_reader = csv.DictReader(umpires_file)
        for row in csv_reader:
            if row['umpire'] in umpire_names and row[' country'] != ' India' and row[' country'] not in umpire_nation:
                umpire_nation[row[' country']] = 1
            if row['umpire'] in umpire_names and row[' country'] != ' India':
                umpire_nation[row[' country']] += 1

    print(umpire_nation)
    plot_umpire_nationalities(umpire_nation)

# Function to plot the collected umpire nationalities
def plot_umpire_nationalities(umpire_nation):
    plt.bar(umpire_nation.keys(), umpire_nation.values())
    plt.show()

# Usage
matches_csv_path = 'archive/matches.csv'
umpires_csv_path = 'archive/umpires.csv'
process_and_plot_umpire_nationalities(matches_csv_path, umpires_csv_path)
