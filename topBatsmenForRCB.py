import csv
import matplotlib.pyplot as plt

# Function to process data and create the pie chart
def process_and_plot_top_batsmen_runs(deliveries_csv_path, team_name, top_n=10):
    # Open the CSV file
    with open(deliveries_csv_path, newline='') as csvfile:
        # Create a DictReader object
        csv_reader = csv.DictReader(csvfile)

        # Create an empty dictionary to store values
        teams_total_score = {}

        # Iterate through the CSV data
        for row in csv_reader:
            if row['batting_team'] == team_name and row['batsman'] not in teams_total_score:
                teams_total_score[row['batsman']] = int(row['batsman_runs'])
            elif row['batting_team'] == team_name:
                teams_total_score[row['batsman']] = teams_total_score[row['batsman']] + int(row['batsman_runs'])

    # Sort the batsmen by total runs in descending order
    sorted_batsmen = sorted(teams_total_score.items(), key=lambda item: item[1], reverse=True)

    # Get the top N batsmen and their runs
    top_batsmen = sorted_batsmen[:top_n]
    x = [item[0] for item in top_batsmen]
    y = [item[1] for item in top_batsmen]

    return x, y

# Function to create and display the pie chart
def plot_top_batsmen_runs(x, y):
    # Create a pie chart to display the data
    plt.pie(y, labels=x, autopct='%1.1f%%')
    plt.show()

# Usage
deliveries_csv_path = 'archive/deliveries.csv'
team_name = 'Royal Challengers Bangalore'
top_batsmen, runs = process_and_plot_top_batsmen_runs(deliveries_csv_path, team_name, top_n=10)
plot_top_batsmen_runs(top_batsmen, runs)
