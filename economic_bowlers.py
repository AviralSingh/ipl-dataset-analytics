import csv
import matplotlib.pyplot as plt

# Function to transform the data
def transform_data(csv_file_path):
    # Initialize dictionaries to store data
    bowlers = {}  # To store runs conceded
    bowlers_overs = {}  # To store the number of overs bowled

    with open(csv_file_path, newline='') as csvfile:
        dataset = csv.DictReader(csvfile)

        for row in dataset:
            # Check if the match is within the specified range
            if 1 <= int(row['match_id']) <= 59:
                # Check if it's not a super over
                if row['bowler'] not in bowlers and row['is_super_over'] != '1':
                    # Initialize the bowler's data if not present
                    bowlers[row['bowler']] = (
                        int(row['total_runs'])
                        - int(row['legbye_runs'])
                        - int(row['bye_runs'])
                    )
                    bowlers_overs[row['bowler']] = 1
                else:
                    # Update the bowler's data if already present
                    bowlers[row['bowler']] += (
                        int(row['total_runs'])
                        - int(row['legbye_runs'])
                        - int(row['bye_runs'])
                    )
                    bowlers_overs[row['bowler']] += 1

    # Calculate the number of overs bowled for each bowler
    for i in bowlers_overs:
        bowlers_overs[i] = bowlers_overs[i] / 6

        # Calculate the average runs conceded per over for each bowler
        bowlers[i] = bowlers[i] / bowlers_overs[i]

    return dict(sorted(bowlers.items(), key=lambda items: items[1]))

# Function to plot the top bowlers
def plot_top_bowlers(data, top_n=10):
    top_bowlers = list(data.keys())[:top_n]
    top_bowler_runs = list(data.values())[:top_n]
    
    plt.plot(top_bowler_runs, top_bowlers, '-or')
    plt.show()

# Usage
csv_file_path = 'archive/deliveries.csv'
bowlers_data = transform_data(csv_file_path)
plot_top_bowlers(bowlers_data)
