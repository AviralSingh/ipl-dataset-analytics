- This Project contains data analysis on IPL dataset
- It contains solutions to specific problems and their solutions along with matplotlib plots
- DictReader is used instead of pandas
- The calculations are done without using numpy library
- Libraries needed to run these programs are mentioned in **requirements.txt** file.
- Install them using
   > pip install **library name**
