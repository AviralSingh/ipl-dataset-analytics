import csv
import matplotlib.pyplot as plt

# Function to transform the data
def transform_data(csv_file_path, match_id_min, match_id_max):
    # Initialize a dictionary to store extra runs per team
    extra_runs_per_team = {}

    with open(csv_file_path, newline='') as csvfile:
        csv_reader = csv.DictReader(csvfile)

        for row in csv_reader:
            match_id = int(row['match_id'])
            if match_id_min < match_id < match_id_max:
                bowling_team = row['bowling_team']
                extra_runs = int(row['extra_runs'])

                if bowling_team not in extra_runs_per_team:
                    extra_runs_per_team[bowling_team] = extra_runs
                else:
                    extra_runs_per_team[bowling_team] += extra_runs

    return extra_runs_per_team

# Function to plot the transformed data
def plot_extra_runs(extra_runs_per_team):
    # Create a plot with team names on the x-axis and extra runs on the y-axis
    plt.plot(extra_runs_per_team.keys(), extra_runs_per_team.values(), '-or')

    # Show the plot
    plt.show()

# Usage
csv_file_path = 'archive/deliveries.csv'
match_id_min = 576
match_id_max = 637

extra_runs_data = transform_data(csv_file_path, match_id_min, match_id_max)
plot_extra_runs(extra_runs_data)
