import csv
import matplotlib.pyplot as plt

# Function to process data and create the plot
def process_and_plot_number_of_matches(matches_csv_path):
    # Open the CSV file to read match data
    with open(matches_csv_path, newline='') as csvfile:
        # Create a DictReader object to read the CSV file as a dictionary
        csv_reader = csv.DictReader(csvfile)

        number_of_matches = {}
        # Create an empty dictionary to store the number of matches per season

        for row in csv_reader:
            # Iterate through each row in the CSV file
            if row['season'] not in number_of_matches:
                # If the season is not in the dictionary, add it with a count of 1
                number_of_matches[row['season']] = 1
            else:
                # If the season is already in the dictionary, increment the count
                number_of_matches[row['season']] += 1

    return number_of_matches

# Function to create and display the bar chart
def plot_number_of_matches(number_of_matches):
    # Create a bar chart with seasons on the x-axis and match counts on the y-axis
    plt.bar(sorted(number_of_matches.keys()), sorted(number_of_matches.values()))
    plt.show()
    # Show the plot

# Usage
matches_csv_path = 'archive/matches.csv'
number_of_matches = process_and_plot_number_of_matches(matches_csv_path)
plot_number_of_matches(number_of_matches)
