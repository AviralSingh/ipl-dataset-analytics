import csv
import matplotlib.pyplot as plt

# Function to process data and create the plot
def process_and_plot_team_wins(matches_csv_path):
    # Open the CSV file
    with open(matches_csv_path, newline='') as csvfile:
        dataset = csv.DictReader(csvfile)

        # Create a dictionary to store the number of wins for each team in each season
        Seasons = {}

        # Iterate through the CSV data
        for row in dataset:
            if row['season'] not in Seasons:
                Seasons[row['season']] = {}
            if row['winner'] not in Seasons[row['season']]:
                Seasons[row['season']][row['winner']] = 1
            else:
                Seasons[row['season']][row['winner']] += 1

    # Sort the seasons by year
    sorted_matches_played = dict(sorted(Seasons.items(), key=lambda x: x[0]))

    # Get the list of teams that participated
    list_teams = []
    for season_data in sorted_matches_played.values():
        for team in season_data.keys():
            if team not in list_teams:
                list_teams.append(team)

    # Create a dictionary where each team maps to their number of wins in each season
    team_wins = {team: [year_data.get(team, 0) for year_data in sorted_matches_played.values()] for team in list_teams}

    return list_teams, sorted_matches_played, team_wins

# Function to create and display the bar chart
def plot_team_wins(list_teams, sorted_matches_played, team_wins):
    # Extract the values from the dictionary
    values = list(team_wins.values())

    # Define positions for the bar chart
    positions = [season for season in range(2008, 2018)]

    # Create the bar chart
    plt.figure(figsize=(10, 5))
    bottom = [0] * len(positions)
    for i in range(len(list_teams)):
        plt.bar(positions, values[i], label=list_teams[i], bottom=bottom)
        bottom = [bottom[j] + values[i][j] for j in range(len(positions))]

    # Add a legend and show the plot
    plt.legend()
    plt.show()

# Usage
matches_csv_path = 'archive/matches.csv'
list_teams, sorted_matches_played, team_wins = process_and_plot_team_wins(matches_csv_path)
plot_team_wins(list_teams, sorted_matches_played, team_wins)
