import csv
import matplotlib.pyplot as plt

# Function to process data and create the plot
def process_and_plot_matches_played(matches_csv_path):
    # Create an empty dictionary to store the number of matches played by each team in each season
    matches_played = {}

    # Read the CSV file
    with open(matches_csv_path) as csvfile:
        csvreader = csv.DictReader(csvfile)

        # Iterate through the CSV data
        for row in csvreader:
            team1 = row['team1']
            team2 = row['team2']
            season = row['season']

            # Update the matches_played dictionary for the first team (team1)
            if team1 in matches_played:
                if season in matches_played[team1]:
                    matches_played[team1][season] += 1
                else:
                    matches_played[team1][season] = 1
            else:
                matches_played[team1] = {season: 1}

            # Update the matches_played dictionary for the second team (team2)
            if team2 in matches_played:
                if season in matches_played[team2]:
                    matches_played[team2][season] += 1
                else:
                    matches_played[team2][season] = 1
            else:
                matches_played[team2] = {season: 1}

    # Merge data for 'Rising Pune Supergiant' and 'Rising Pune Supergiants'
    matches_played['Rising Pune Supergiant'].update(matches_played['Rising Pune Supergiants'])
    del matches_played['Rising Pune Supergiants']

    # Extract the list of team names and sorted seasons
    teams = list(matches_played.keys())
    seasons = [str(season) for season in range(2008, 2018)]

    return teams, seasons, matches_played

# Function to create and display the grouped bar chart
def plot_matches_played(teams, seasons, matches_played):
    # Create a figure and axis for the plot
    fig, ax = plt.subplots(figsize=(15, 3))

    # Initialize the 'bottom' list to keep track of the bottom position for each team's bars
    bottom = [0] * len(teams)

    # Iterate through seasons to create a grouped bar chart
    for season in seasons:
        # Get the number of games played by each team for the current season
        games_played = [matches_played[team].get(season, 0) for team in teams]

        # Create a bar chart for the current season and update the 'bottom' positions
        ax.bar(teams, games_played, label=season, bottom=bottom)
        bottom = [sum(x) for x in zip(bottom, games_played)]

    # Set labels, title, and rotate the team names for better visibility
    ax.set_ylabel('Number of Games Played')
    ax.set_xlabel('Teams')
    ax.set_title('Number of Matches Played by Each IPL Team by Season')
    plt.xticks(rotation=30)

    # Add a legend to distinguish seasons and adjust its position
    ax.legend(title='Seasons', loc='upper left', bbox_to_anchor=(1, 1))

    # Display the plot
    plt.show()

# Usage
matches_csv_path = 'archive/matches.csv'
teams, seasons, matches_played = process_and_plot_matches_played(matches_csv_path)
plot_matches_played(teams, seasons, matches_played)
