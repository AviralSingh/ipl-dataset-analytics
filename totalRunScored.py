import csv
import matplotlib.pyplot as plt

# Function to process data and create the bar chart
def process_and_plot_total_runs_by_team(deliveries_csv_path):
    # Open the CSV file
    with open(deliveries_csv_path, newline='') as csvfile:
        # Create a DictReader object to read the CSV data
        csv_reader = csv.DictReader(csvfile)

        # Create an empty dictionary to store the total runs scored by each team
        teams_total_score = {}

        # Iterate through the CSV data
        for row in csv_reader:
            # Check if the batting team is already in the dictionary
            if row['batting_team'] not in teams_total_score:
                # If not, add the batting team to the dictionary with the total runs
                teams_total_score[row['batting_team']] = int(row['total_runs'])
            else:
                # If the team is already in the dictionary, add the total runs to the existing value
                teams_total_score[row['batting_team']] += int(row['total_runs'])

    # Merge 'Rising Pune Supergiant' and 'Rising Pune Supergiants' into one entry
    teams_total_score['Rising Pune Supergiant'] += teams_total_score.get('Rising Pune Supergiants', 0)
    if 'Rising Pune Supergiants' in teams_total_score:
        del teams_total_score['Rising Pune Supergiants']

    return teams_total_score

# Function to create and display the bar chart
def plot_total_runs_by_team(teams_total_score):
    # Define the list of teams and their corresponding color styles for the bar chart
    teams = ['SRH', 'RCB', 'MI', 'RSP', 'GL', 'KKR', 'KXIP', 'DD', 'CSK', 'RR', 'DC', 'Kochi Tuskers', 'Pune Warriors']
    cstyle = ['darkorange', 'darkred', 'blue', 'purple', 'orange', 'violet', 'red', 'cyan', 'yellow', 'turquoise', 'indigo', 'purple', 'gold']

    # Create a bar chart to show the total runs scored by each team
    plt.bar(teams, teams_total_score.values(), color=cstyle)

    # Define fonts for the labels and title
    fonts = {'family': 'serif', 'color': 'darkred', 'size': 20}

    # Add labels and title to the plot
    plt.xlabel('Teams', fontdict=fonts)
    plt.ylabel('Total Runs', fontdict=fonts)
    plt.title('Total Runs Scored by Each Team over the History of IPL', fontdict=fonts)
    
    # Display the plot
    plt.show()

# Usage
deliveries_csv_path = 'archive/deliveries.csv'
teams_total_score = process_and_plot_total_runs_by_team(deliveries_csv_path)
plot_total_runs_by_team(teams_total_score)
